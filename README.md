Livro:Finanças Matemática: Teoria e Prática

AUTOR

Marco Aurélio Palumbo Cabral (Professor do Instituto de Matemática da UFRJ)

DESCRIÇÃO

Um livro introdutório de Finanças Matemática para alunos de graduação com muito pouco pré-requisito.
O público alvo deste livro são alunos de 3o período em diante de curso de ciências exatas,
incluindo Engenharia, Física, Computação, Matemática, Estatística, Atuaria.
Os pré-requisitos são apenas maturidade matemática, pois o material é autocontido. 
Buscamos juntar teoria com prática, com foco em aplicações no mercado brasileiro.
Além da teoria matemática completa, com apresentação de modelos, teoremas e provas,
apresentamos métodos numéricos para implementar estes modelos.
Utilizado na UFRJ (Universidade Federal do Rio de Janeiro) e em várias instituições pelo Brasil.


SÍTIOS

Visite https://sites.google.com/matematica.ufrj.br/mapcabral/livros-e-videos para ter acesso a outros materiais relacionados a este livro.

HISTÓRIA DAS VERSÕES

Primeira Edição de 07 de Agosto de 2020.

LICENÇA

Creative Commons CC BY-NC-SA 3.0 BR, Atribuição (BY) Uso Não-Comercial (NC) CompartilhaIgual (SA) 3.0 Brasil.
Veja arquivo LICENSE para detalhes.

