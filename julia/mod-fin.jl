#!/usr/bin/julia
#import Pkg; Pkg.add("Gadfly")
#To Run inside julia:
#include("mod-fin.jl")


################################
# Algoritmo 1: Escala Temporal
################################
function escala_temp_JR(dt0, r_dia, mu_dia, sigma_dia)
  	if (mu_dia <= sigma_dia) 
	    println("Parâmetros inconsistentes");
	    return -1;
	end
	M0= (1+r_dia)/mu_dia
	M1= 1 +(sigma_dia/mu_dia)^2;
	dt=dt0;
	while (1-M0^dt)^2 >= M1^dt -1 
		dt=dt/2;
	end
# Calculando up, down e r no intervalo ∆t
	mu_dt= (mu_dia)^dt;
	sigma_dt=sqrt( (mu_dia^2 + sigma_dia^2)^dt - mu_dia^(2*dt));
	u_dt = mu_dt + sigma_dt;
	d_dt = mu_dt - sigma_dt;
	r_dt= (1+r_dia)^dt -1;
	return dt, u_dt, d_dt, r_dt;
end
function display_escala_temp_JR(dt0, r_dia, mu_dia, sigma_dia)
	println("ENTRADA:");
	println("dt0,       r_dia,       mu_dia,      sigma_dia");
        println(dt0, "      ",r_dia,"%            ", mu_dia,"         ", sigma_dia);
	p= escala_temp_JR(dt0, r_dia, mu_dia, sigma_dia);
	println("SAIDA:");
	println("dt,       u_dt,               d_dt,           r_dt");
	println(p);
	return p;
end

#############################
# Definindo PAYOFFS das Opções
###########################
function exotico(S,K)
 	return S^2;
end
function put(S,K)
   if (K-S)<0
       return 0;
   else
       return K-S;
   end
end
function call(S,K)
   if (S-K)<0
       return 0;
   else
       return S-K;
   end
end

################################
# Algoritmo com valor que depende de toda a árvore.
################################


################################
# Algoritmo 3: Precificando Opção Americana: Método Direto
# Tempo t=0 até T=N
################################
#Teste: opcao_americana(2, 4, 2, 0.5, 0.25, put, 5) =  1.36
# N=2,  S0=4, u=2=1/d, r=0.25. Com payoff PUT K=5
#
function opcao_americana(N, S0, u, d, r, payoff, strike)
    # Testando ausência de arbitragem
    if d>=1+r || u<=1+r
	    println("Existe Arbitragem");
	    return -1;
    end
    p=(1+r-d)/(u-d);
    q=1-p;
    # Vetor com valores da opção
    V=zeros(N+1);
    # Valor de V_N dado pelo Payoff
    for j=0: N
       V[j+1]=payoff(d^j*u^(N-j)*S0, strike);
    end
    # Voltando até o tempo zero na árvore
    for j= reverse(0:N-1)
        # Calculando E_j[V_{j+1]]/(1+r)
     	for i=0: j
	   V[i+1]=max(payoff(d^i*u^(j-i)*S0, strike), (p*V[i+1]+q*V[i+2])/(1+r));
	end
    end
    return V[1];
end

function mostrarSV(i,N,S,V)
   print("H^",N-i," T^",i," \t", S, "\t",V);
end
######################
# Usada em opcao_americana e opcao_europeia
# Gera os valores de execução no tempo final e imprime os valores na tela caso a
# mostrar>0
######################
function inicializa_opcao(N, S0, u, d, r, payoff, strike, mostrar)
    if mostrar>0
        p=(1+r-d)/(u-d); q=1-p;
	print("N= ", N, "    S0=",S0, " u=",u," d=",d,"  r=",100*r,"%");
	print("  strike= ", strike, "\np= ",p,"\n"); 
	println("-------------------------------------------------"); 
	println("   w\t\tS_",N,"\tV_",N);
    end
    V=zeros(N+1);
    for j=0: N
       V[j+1]=payoff(d^j*u^(N-j)*S0, strike); 
       if V[j+1]< 0
          V[j+1]=0;
       end
       if mostrar>0
	   #println("j= ",j,"  S[j+1]=",d^j*u^(N-j)*S0,  "  V[j+1]=",V[j+1], "  H^",N-j," T^",j);
	   mostrarSV(j,N, d^j*u^(N-j)*S0,V[j+1]);
	   println("");
	end
    end
    return V;
end
##################################################
# Mesma função mas com opção binaria mostrar=0 ou 1.
# Mostra toda a árvore de valores da opção, incluindo quando ocorre execução da
# opção americana no meio da árvore.
##################################################
#Teste: opcao_americana(2, 4, 2, 0.5, 0.25, put, 5, 1) =  1.36
# N=2,  S0=4, u=2=1/d, r=0.25. Com payoff PUT K=5
#
# opcao_europeia(2, 4, 2, 0.5, 0.25, put, 5, 1) =  0.96
#
# Outro Teste: opcao_americana(3, 100, 1.1, 0.9, exp(0.05)-1, put,100,1)=2.738850558307428
# Retirado do Livro The Mathematics of Finance; Victor Goodman e Joseph Stampli
# p 52, 53 e 54.
#  Mesma europeia: opcao_europeia(3, 100, 1.1, 0.9, exp(0.05)-1, put,100,1) = 1.6010577694145198
#
function opcao_americana(N, S0, u, d, r, payoff, strike, mostrar)
    if d>=1+r || u<=1+r
	    println("Existe Arbitragem");
	    return -1;
    end
    p=(1+r-d)/(u-d); q=1-p;
    V= inicializa_opcao(N, S0, u, d, r, payoff, strike, mostrar)
    for j= reverse(0:N-1)
        if mostrar>0
  	   println("-------------------------------------------------");
	   println("   w\t\tS_",j,"\tV_",j);
	end
     	for i=0: j
	   valor_intrinseco=payoff(d^i*u^(j-i)*S0, strike);
	   valor_esperado=(p*V[i+1]+q*V[i+2])/(1+r);
	   novo_valor=max(valor_intrinseco, valor_esperado);
           if mostrar>0
	      mostrarSV(i,j, d^i*u^(j-i)*S0, novo_valor);
	       if valor_intrinseco > valor_esperado 
		       println(" EXECUTAR OPÇÃO: E[V_{n+1}]/(1+r)=", valor_esperado);
	       else
	          println("    Payoff imediato= ", valor_intrinseco);
	       end
	   end
	   V[i+1]=novo_valor;
	end
    end
    if mostrar>0
	println("-------------------------------------------------");
	println("Valor da Opção Americana: ",V[1]);
    end
    return V[1];
end
#Testando:
#opcao_americana(3, 4, 2, 0.5, 0.1, payoff,1)

################################
# Algoritmo 3: Precificando Opção Europeia: Método Direto
# Tempo t=0 até T=N
################################
# Teste:
#    N=2,  S0=4, u=2=1/d, r=0.25. Com payoff PUT K=5
#    opcao_europeia(2, 4, 2, 0.5, 0.25, put, 5) =  0.96
function opcao_europeia(N, S0, u, d, r, payoff, strike)
    # Testando ausência de arbitragem
    if d>=1+r || u<=1+r
	    println("Existe Arbitragem");
	    return -1;
    end
    p=(1+r-d)/(u-d);
    q=1-p;
    # Vetor com valores da opção
    V=zeros(N+1);
    # Valor de V_N dado pelo Payoff
    for j=0: N
       V[j+1]=payoff(d^j*u^(N-j)*S0, strike);
    end
    # Voltando até o tempo zero na árvore
    for j= reverse(0:N-1)
        # Calculando E_j[V_{j+1]]/(1+r)
     	for i=0: j
	   V[i+1]=(p*V[i+1]+q*V[i+2])/(1+r);
	end
    end
    return V[1];
end
##################################################
# Mesma função mas com opção binaria mostrar=0 ou 1.
# Mostra toda a árvore de valores da opção
##################################################
#    N=2,  S0=4, u=2=1/d, r=0.25. Com payoff PUT K=5
#    opcao_europeia(2, 4, 2, 0.5, 0.25, put, 5,1) =  0.96
function opcao_europeia(N, S0, u, d, r, payoff, strike, mostrar)
    if d>=1+r || u<=1+r
	    println("Existe Arbitragem");
	    return -1;
    end
    p=(1+r-d)/(u-d); q=1-p;
    V= inicializa_opcao(N, S0, u, d, r, payoff, strike, mostrar)
    for j= reverse(0:N-1)
        if mostrar>0
  	   println("-------------------------------------------------");
	   println("   w\t\tS_",j,"\tV_",j);
	end
     	for i=0: j
	   V[i+1]=(p*V[i+1]+q*V[i+2])/(1+r); 
           if mostrar>0
	      mostrarSV(i,j, d^i*u^(j-i)*S0, V[i+1]);
	      println("");
	   end
	end
    end
    if mostrar>0
	println("-------------------------------------------------");
	println("Valor da Opção Europeia: ",V[1]);
    end
    return V[1];
end
################################ 
# Algoritmo 4: Monte Carlo
################################

###################
# Gera Caminho
###################
# Caminho com u,d, r fixos.
# Gera um caminho de S(0) até S(N) com N+1 entradas
####################
function gera_caminho_simples(N, S0, u, d, r)
    # Testando ausência de arbitragem
    if d>=1+r || u<=1+r
	    println("Existe Arbitragem");
	    return -1;
    end
    p=(1+r-d)/(u-d); 
    s=zeros(N+1); s[1]=S0;
    m=rand(N); # gerando N lançamentos de moeda
    D=1/(1+r)^(N);
    for i=2: N+1
        if m[i-1]<p 
	    s[i] = u*s[i-1];
	else
	    s[i] = d*s[i-1];
	end
    end 
    # vetor com a série temporal e o desconto D.
    return s, D;
    # Using Plots; plot(s); plot!(s); cumulativo
end

# Assume que pode-se executar antecipadamente
#
function payoff_caminho_put_americano_dez(s)
   # call com strike K
   K=10;
   valor= maximum(s - K*ones(length(s)));
   if valor >0 
      return valor;
   else
      return 0;
   end;
end



##########################################################
#  Monte Carlo Europeu: O payoff_caminho depende de S, toda a história dos
#  valores da funções.
##########################################################
#using RollingFunctions
#result = rollmean(data, windowsize);
# Testando Monte Carlo Europeu
#  monte_carlo(10000, 3, 8, 1.5, 0.5, 0, payoff_caminho_put_dez)
#  O resultado (exato) 4.125
# Alguns obtidos (com 1000): 4.109, 3.983, 4.131, 4.07)
# Alguns obtidos (com 10000): 4.1613 4.0816, 4.1467, 4.1177, 4.1154, 
#
# Testando outro payoff
# monte_carlo(100000, 3, 8, 1.5, 0.5, 0, payoff_caminho_asiatico_put) = 3.00253, 3.0047675
#  Mais barato.
#
# ALguns Payoffs de caminho para testar
#
function payoff_caminho_put_dez(s)
   return put(s[end],10)
end
function payoff_caminho_asiatico_put(s)
   # call com strike K
   K=10;
   valor= put(mean(s),K);
end
function monte_carlo(MAX, N, S0, u,d,r, payoff_caminho)
    soma=0; soma2=0; m=zeros(MAX);
    for j= 1:MAX
       s,D = gera_caminho_simples(N, S0, u, d, r);
       m[j] = payoff_caminho(s)*D;
      # soma += m[j];
      # soma2 += m[j]^2;
    end
    return mean(m);
    # determinando windowsize
    window =512;
    if MAX < 10
        window =1;
    elseif  MAX < 50
        window=5;
    elseif  MAX < 200
        window=30;
    elseif while MAX - window < 50
              window = div(window,2);
       end
    end
    # Dado os resultados do Monte Carlo, gera uma série de médias móveis
    # com os valores, com janela padrão de 512.
   result = rollmean(m, window);
  # println("m=",m);
 #  print("result=",result);
   print("Window Size =", window);
   println("  media =", mean(result), "  std=", std(result));
  return mean(m), std(m), soma/MAX, sqrt((soma2/MAX-(soma/MAX)^2));
end


# Teste do método de Monte Carlo usando o exemplo do meu livro
# Exemplo do MEU Livro exemplo 2.7  do LIvro p.40): r =0, u=1.5,  d=0.5, S0=8, T=3, put, K=10
# PUT Strike K=10, T=3;
#    valor(3,     3, 8, 0, 1, 0.5); # = 4.125 = 33/8 

function testa_MC_simples1(MAX)
# Exemplo do MEU Livro exemplo 2.7  do LIvro p.40): 
    N=3; 
    S0=8;  
    r=0/100;
    u=1.5;  
    d=0.5; 
    valor=monte_carlo(MAX, N, S0,u,d,r, payoff_caminho_put_dez)
    valor_exato= opcao_europeia(N, S0, u, d, r, put, 10)
    println("MAX= ",MAX,"  Calculado= ",valor, "   Exato= ", valor_exato,  " ERRO rel= ", abs(valor-valor_exato)/valor_exato);
    return valor;
end

# Da pag. 37, exemplo 2.6 do livro
# Call strike 3, S0=2, u=2, d=0.5, r= 1/4; T=2
# Valor 4/5
function testa_MC_simples2(MAX)
# Exemplo do MEU Livro exemplo 2.7  do LIvro p.40): 
    N=2;
    S0=2;  
    r=1/4;
    u=2.0;  
    d=0.5; 
    function payoff_caminho_call_tres(s)
       return call(s[end],3)
    end
    valor=monte_carlo(MAX, N, S0,u,d,r, payoff_caminho_call_tres)
    valor_exato= opcao_europeia(N, S0, u, d, r, call, 3)
    println("MAX= ",MAX,"  Calculado= ",valor, "   Exato= ", valor_exato,  " ERRO rel= ", abs(valor-valor_exato)/valor_exato);
    return;
end


####################
# Exemplo do Shreeve Vol.1 (para testar os algoritmos)
# Lookback p. 14
####################
#N=3; S0=4;u=2;d=0.5;r=1/4;
#Payoff max(S_n) -S_3
#Valor= 1.376
function testa_MC_simples3(MAX)
    N=3; S0=4;u=2;d=0.5;r=1/4;
    function payoff_lookback(s)
       return maximum(s)-s[end];
    end
    valor=monte_carlo(MAX, N, S0,u,d,r, payoff_lookback)
    valor_exato= 1.376;
    println("MAX= ",MAX,"  Calculado= ",valor, "   Exato= ", valor_exato,  " ERRO rel= ", log10(abs(valor-valor_exato)/valor_exato));
    return;
end

function testa_MC_simples4(MAX)
# Exemplo com numeros quaisquer
# Exemplo monte-carlo1.txt
    N=50; S0=2;  r=0.04; u=1.10;  d=0.8; K=10;
# Exemplo monte-carlo2.txt
#    N=20; S0=2;  r=0.04; u=1.10;  d=0.8; K=3;
    function payoff_caminho_call_tres(s)
       return call(s[end],K)
    end
    function payoff(s)
       return call(s,K)
    end
    valor=monte_carlo(MAX, N, S0,u,d,r, payoff_caminho_call_tres);
    valor_exato= opcao_europeia(N, S0, u, d, r, call, 10);
    erro_rel=abs(valor-valor_exato)/valor_exato;
    println("MAX= ",MAX,"  Calculado= ",valor, "   Exato= ", valor_exato,  " ERRO rel= ", erro_rel);
    println("N= ",N, " K= ",K, "L = ", 1);
    return erro_rel;
end
linreg(x, y) = hcat(fill!(similar(x), 1), x) \ y;

function test_MC(M)
   erro=zeros(M);
   logerro=zeros(M);
   L=1;
   for i=1: M
       # Calcular média das médias 
       for j=1: L
	   erro[i]+=testa_MC_simples4(20*2^i);
       end
       logerro[i]=log2(erro[i]/L);
       println("\n\ni  M      ErroRel");
       for j=1: i
	  println(j, " ", 20*2^j, " ", erro[j]/L);
       end
       println("Regressão LInear: ",linreg(1:i, logerro[1:i]));
   end
end


####################
# Gerar Random Walk
####################

function gerar_random_walk(N)
    m=rand(N); # gerando N lançamentos de moeda
    s=zeros(N+1); s[1]=0; p=1/2;
    m=rand(N); # gerando N lançamentos de moeda
    for i=2: N+1
        if m[i-1]<p
	    s[i] = s[i-1] +1;
	else
	    s[i] = s[i-1] -1;
	end
    end 
    return s;
end
#using Plots;
function imprimir_rw(N)
    s1=gerar_random_walk(N);
    s2=gerar_random_walk(N);
    s3=gerar_random_walk(N);
    s4=gerar_random_walk(N);
    println("t  s1  s2  s3   s4");
    for i=1: N+1
       println(i-1, "  ", s1[i], " ", s2[i], " ", s3[i], " ", s4[i], " ");
    end
#    plot(s1);
#    plot!(s2);
#    plot!(s3);
#    plot!(s4);
end

####################################################
# Modelo CRR
####################################################

#################################################
# Diversas formas de calcular sigma_neutro_dia 
################################################
# NAO DEU CERTO!
function calcula_sigma_neutro_dia(dt, rdia, u,d,r)
   # rdia=(1+r)^(1/dt)-1;
    s1=sqrt( ((1+rdia)^dt*(u+d) - u*d)^(1/dt) - (1+rdia)^2);
    s2=sqrt( ((1+r)*(u+d) - u*d)^(1/dt) - (1+rdia)^2);
    p=(1+r-d)/(u-d);
    s3= sqrt((p*u^2 + (1-p)*d^2)^(1/dt)-(1+rdia)^2);
    println(s1," ",s2, " ", s3, "\n  Diferenca ", s1-s2, " ", s2 - s3);
   return s3;
end


######################################
#  Fórmula de Black & Scholes
######################################
# Para poder usar precisa do pacote distribution.
# Atualmente para poder funcionar precisei linkar
#  ln -s /usr/lib/x86_64-linux-gnu/libopenblas.so /usr/lib/x86_64-linux-gnu/libopenblas64_.so.0 

# import Pkg; Pkg.add("Distributions")
#using Distributions
# Used by cdf (normal)
function meu_cdf(x)
   # cumulative distribution function da normal
   return cdf(Normal(0,1), x)
end
# Teste da Rotina
# BS_euro_vanilla_call(50, 100, 1, 0.05, 0.25)  = 0.027352509369436617
function BS_euro_vanilla_call(S, K, T, r, sigma)
    #S: spot price #K: strike price #T: time to maturity #r: interest rate #sigma: volatility of underlying asset
    d1 = (log(S / K) + (r + 0.5 * sigma^2) * T) / (sigma * sqrt(T));
    d2 = (log(S / K) + (r - 0.5 * sigma^2) * T) / (sigma * sqrt(T));
    return (S * meu_cdf(d1) - K * exp(-r * T) * meu_cdf(d2));
end
# Teste da Rotina
# BS_euro_vanilla_put(50, 100, 1, 0.05, 0.25) =   45.15029495944084
function BS_euro_vanilla_put(S, K, T, r, sigma)
    #S: spot price #K: strike price #T: time to maturity #r: interest rate #sigma: volatility of underlying asset
    d1 = (log(S / K) + (r + 0.5 * sigma^2) * T) / (sigma * sqrt(T));
    d2 = (log(S / K) + (r - 0.5 * sigma^2) * T) / (sigma * sqrt(T));
    return (K * exp(-r * T) * meu_cdf(-d2) - S * meu_cdf(-d1));
end

################################
# Usando todas as rotinas
################################
# Entre com o tempo final e discretização. Calculamos os dados e o valor do
# contrato
function valor_put(N, T, S0, r_dia, mu_dia, sigma_dia, strike)
   K=strike;
   dt0=T/N;  
   dt, u_dt, d_dt, r_dt= display_escala_temp_JR(dt0, r_dia, mu_dia, sigma_dia);
   Nusado=trunc(Int,T/dt);
   println("dt =", dt, "  Nusado=",Nusado, "   N=",N);
   println(opcao_europeia(Nusado, S0, u_dt, d_dt, r_dt, put,K));
   # Para compararmos
   sigma_neutro= calcula_sigma_neutro_dia(dt, r_dia, u_dt,d_dt,r_dt);
   println(BS_euro_vanilla_put(S0, K, T, r_dia, sigma_neutro));
   println(BS_euro_vanilla_put(S0, K, T, r_dia, sigma_dia));
   return ;
end
##################################################################
# Test rotinas
##################################################################
function testa_es(T,N,r_dia,mu_dia,sigma_dia)
   dt0=T/N;  
   T=trunc(Int,T);
   dt, u, d, r= escala_temp_JR(dt0, r_dia, mu_dia, sigma_dia)
   N=trunc(Int,T/dt);
   println("T=",T, " N=",N, "   dt=",dt);
   # Calcula media e desvio padrão.
   p=1/2;
   media=0; 
   segmom=0;
   for i=0: N
      print("  --- i=",i, " ", u^i*d^(N-i), " ", p^i*(1-p)^(N-i));
      media+=(p*u)^i*((1-p)*d)^(N-i)*binomial(N,i);
      segmom+=(p*u^2)^i*((1-p)*d^2)^(N-i)*binomial(N,i);
   end
   desvpad=sqrt(segmom-(media)^2)
   println("\n Média ", media, "  Desv Padrão ",desvpad);
   println("ERROS: Media ",media-mu_dia, "  Desv Pad ", desvpad-sigma_dia);
end
function exemplo_meu_livro_2_7_refinado()
   K=10; T=3;mu_dia=1; sigma_dia=0.5;S0=8;
    r=0;u=1.5;d=0.5;
    #p=(1+r-d)/(u-d);
    p=0.5;
    mu_dia=p*u+(1-p)*d;
    sigma_dia=(u-d)*sqrt(p*(1-p));
    println("up =",u, " down=",d, "  p=",p, "  mu_dia=",mu_dia, "
    sigma_dia=",sigma_dia);
    for N in 3*[1, 2, 2^2, 2^3, 2^4, 2^5, 2^6, 2^7, 2^8, 2^9, 2^10, 2^11,2^12, 2^13, 2^14 ]

       dt0=T/N;  
       dt, u_dt, d_dt, r_dt= display_escala_temp_JR(dt0, r, mu_dia, sigma_dia);
       Nusado=trunc(Int,T/dt);
#       println("dt =", dt, "  Nusado=",Nusado, "   N=",N);
       println("aa ", Nusado, "  ",opcao_europeia(Nusado, S0, u_dt, d_dt, r_dt, put, K));
    end
    println("--------------------------------------------------");
    println("r=10%");
    println("--------------------------------------------------");
    r=0.1;
    u=1.5;d=0.5;
 #    p=(1+r-d)/(u-d);
    p=0.5;
    mu_dia=p*u+(1-p)*d;
    sigma_dia=(u-d)*sqrt(p*(1-p));
    println("up =",u, " down=",d, "  p=",p, "  mu_dia=",mu_dia, "
    sigma_dia=",sigma_dia);
    for N in 3*[1, 2, 2^2, 2^3, 2^4, 2^5, 2^6, 2^7, 2^8, 2^9, 2^10, 2^11,2^12, 2^13, 2^14 ]
       dt0=T/N;  
       dt, u_dt, d_dt, r_dt= display_escala_temp_JR(dt0, r, mu_dia, sigma_dia);
       Nusado=trunc(Int,T/dt);
#       println("dt =", dt, "  Nusado=",Nusado, "   N=",N);
       println("aa ", Nusado, "  ",opcao_europeia(Nusado, S0, u_dt, d_dt, r_dt, put, K));
    end
end

function exemplo_put_americano_meu_livro_refinado()
   K=20; T=3;mu_dia=1; sigma_dia=0.5;S0=16;
    r=0.1;
    u=1.5;d=0.5;
 #    p=(1+r-d)/(u-d);
    p=0.5;
    mu_dia=p*u+(1-p)*d;
    sigma_dia=(u-d)*sqrt(p*(1-p));
    println("--------------------------------------------------");
    println("S0=",S0, " u=",u, " d=",d, " r=",r," K=",K, "  p=",p, "  mu_dia=",mu_dia, "
    sigma_dia=",sigma_dia);
    println("--------------------------------------------------");
    for N in 3*[1, 2, 2^2, 2^3, 2^4, 2^5, 2^6, 2^7, 2^8, 2^9, 2^10, 2^11,2^12, 2^13, 2^14 ]
       dt0=T/N;  
       dt, u_dt, d_dt, r_dt= display_escala_temp_JR(dt0, r, mu_dia, sigma_dia);
       Nusado=trunc(Int,T/dt);
#       println("dt =", dt, "  Nusado=",Nusado, "   N=",N);
       println("aa ", Nusado, "  ",opcao_americana(Nusado, S0, u_dt, d_dt, r_dt, put, K));
    end
end

##################################################################
# Exemplo do MEU Livro exemplo 2.7  do LIvro p.40): r =0, u=1.5,  d=0.5, S0=8, T=3, put, K=10
# PUT Strike K=10, T=3, S0=8 r=0% sigma=0.5  mu=1
function exemplo_meu_livro_2_7()
   strike=10
# Referencia    BS_euro_vanilla_put(8, 10, 3, 0, 0.5) =4.107933747613639
#    (a)
    valor_put(3,     3, 8, 0, 1, 0.5, strike); # = 4.125 = 33/8 
    valor_put(45000, 3, 8, 0, 1, 0.5, strike); # = 3.9571377166213253
#    (b)
    # PUT Strike K=10, T=3, S0=8 r=10% sigma=0.5  mu=1
# Referencia    BS_euro_vanilla_put(8, 10, 3, 0.1, 0.5); # = 2.294450493878132
    valor_put(3, 3, 8, 0.1, 1, 0.5, strike); # = 2.2719759579263696 (exemplo 2.7  do LIvro p.40)
end

####################################################
# Modelo CRR
####################################################
function CRR(m, sigma)
   alfa=1/2*(m+(1+sigma^2)/m);
   u=alfa+sqrt(alfa^2-1);
   d=alfa-sqrt(alfa^2-1);
   d=1/u;
   p=(m-d)/(u-d);
   print("u=",u);
   print("  d=",d);
   println("  p=",p);
   println("  ud=",u*d);
   print("  m=",m);
   println("  sigma=",sigma);
   print("  média=",p*u+(1-p)*d);
   print("  stedv=",sqrt((m-d)*(u-m)), " ou ",(u-d)*sqrt(p*(1-p)));
   return u,d,p;
end

function aproxima_pi(N)
   pi_p=2;
   for i=1:N
      pi_p*= (4*i^2)/(4*i^2-1);
      #pi_p*= (2*i)*(2*i)/(2*i-1)/(2*i+1);
   end
   #println(abs(pi-pi_p)/pi);
   return pi_p;
end

#using Plots;
#using PyPlot;
#import Pkg; Pkg.add("PyPlot")
function erro_com_n(N)
  erros=zeros(N);
  for i=1:N
     erros[i]=log(abs(aproxima_pi(2^i)-pi));
     #plot!(i, log(abs(aproxima_pi(2^i)-pi)));
  end
  PyPlot.plot(erros);
  println(erros);
  println(linreg(1:N, erros));
end

#import Pkg; Pkg.add("Plots")
#    using Plots

#  plot(x,(y[x,2]-y[x,1]))

#######################################
# EXECUTAR
#exemplo_meu_livro_2_7();
#exemplo_meu_livro_2_7_refinado()



#imprimir_rw(1000);
#testa_MC_simples4(300)

