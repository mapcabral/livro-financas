# Compara *tex do diretorio atual com o do DIR

DIR=usb/LIVRO-ModMatFin
DATE=`date +%Y-%b-%d-%a-%Hh`
#DATE=`date +%Y-%b-%d`
LISTALIVRO=lista-livros-$(DATE).txt

default: compilar

compilar:
	texfot pdflatex -file-line-error -halt-on-error -interaction nonstopmode LivroFinancasMatematica.tex
comparar:
#	echo  $(DATE)
#	echo off
#	find  -maxdepth  1  -name "*tex" -ls -exec diff --brief  {} $(DIR) \;
	find  julia/*jl -maxdepth  1   -ls -exec diff --brief  {} $(DIR)/julia \;
	find -name "FinMat-*tex" -ls -exec diff --brief  {} $(DIR) \;
	find -name "*txt" -ls -exec diff --brief  {} $(DIR) \;
#	diff --brief   LivroFinancasMatematica.tex $(DIR)
tgz: 
	touch ultimo-backup-$(DATE)
	cd ..; tar czvf livro-financas-$(DATE).tgz  LIVRO-ModMatFin/by-nc-nd.png LIVRO-ModMatFin/Standard*png LIVRO-ModMatFin/Makefile LIVRO-ModMatFin/*txt LIVRO-ModMatFin/Session.vim LIVRO-ModMatFin/*tex LIVRO-ModMatFin/*.ods LIVRO-ModMatFin/julia/* 
#	cd ..; tar czvf livro-financas-$(DATE).tgz  --exclude "*.pdf" --exclude "*.ps" --exclude "*.toc" --exclude "*.dvi" --exclude "*.aux" --exclude "*.out"--exclude "*.log"  --exclude "*~"  --exclude "*.idx" --exclude "*.ilg"   --exclude "*.swp"  LIVRO-ModMatFin/
	ls -lh ../livro-financas*.tgz  
	mv    -i ../livro-financas*tgz tgzs
	ls -lh tgzs/*



